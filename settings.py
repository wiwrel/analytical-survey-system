from surveys.PQV import PQV
from surveys.FFMQ import FFMQ
from surveys.SAQ import SAQ
from surveys.MBI import MBI
from graph.graffitist import PqvPlot, SaqPlot, FfmqPlot, MbiPlot
from report.reporter import PQVReporter, FFMQReporter

pqv_set_up = {
    'width': 20,
    'height': 10,
    'text_base': 'templates/texts/PQV',
    'text_map': {'Conf': 'Conformity.txt',
                 'Tra': 'Tradition.txt',
                 'Dobr': 'Benevolence.txt',
                 'Uni': 'Universalism.txt',
                 'Sam': 'Self_Direction.txt',
                 'Sti': 'Stimulation.txt',
                 'Ged': 'Hedonism.txt',
                 'Dost': 'Achievement.txt',
                 'Vlas': 'Power.txt',
                 'Bez': 'Security.txt'},
    'image_tpl': 'templates/images/schvarts.jpg',
    'text_pic_sign': 'Pic_one_sign.txt',
    'text_prefix': 'Prefix.txt'
}

ffmq_set_up = {
    'width': 20,
    'height': 10,
    'text_base': 'templates/texts/FFMQ',
    'text_map': {'description': 'Describing.txt',
                 'impartial_relation_to_exp': 'Nonjudging.txt',
                 'non_responsive': 'Nonreactivity.txt',
                 'observation': 'Observing.txt',
                 'action_understanding': 'Wareness.txt'},
    'image_tpl': None,
    'text_pic_sign': None,
    'text_prefix': None
}

settings = [
    {'survey': 'pqv',
     'offset': 6,
     'last_column': 102,
     'handler': PQV,
     'graph': PqvPlot,
     'report': (PQVReporter, pqv_set_up)
     },
    {'survey': 'saq',
     'offset': 5,
     'last_column': 52,
     'handler': SAQ,
     'graph': SaqPlot,
     'report': None
     },
    {'survey': 'ffmq',
     'offset': 4,
     'last_column': 43,
     'handler': FFMQ,
     'graph': FfmqPlot,
     'report': (FFMQReporter, ffmq_set_up)
     },
    {'survey': 'mbi',
     'offset': 4,
     'last_column': 25,
     'handler': MBI,
     'graph': None,
     'report': None
     }
]
