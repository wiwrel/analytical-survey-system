import os
import abc
from fpdf import FPDF
from copy import copy
from models.report_model import SettingsReport, Font


class PDF(FPDF):

    STATIC_FONT = 'fonts/DejaVuSans.ttf'
    STATIC_FAMILY = 'Dejavu'

    def header(self, title='', size: int = 24):
        self.dejavu(size=size)

        if isinstance(title, str):
            self.cell(w=10, h=10, txt=title, ln=1)

        if isinstance(title, tuple):
            for string in title:
                self.cell(w=10, h=10, txt=string, ln=1)

    def dejavu(self, size: int, font_family: str = STATIC_FAMILY, font_path: str = STATIC_FONT):
        self.add_font(family=font_family, fname=font_path, uni=True)
        self.set_font(family=font_family, size=size)

    def insert_image(self, path: str = '', description: str = None, pos_x: int = None, pos_y: int = None, w: int = 0,
                     h: int = 0, width: int = 20, height: int = 10):
        self.image(name=path, x=pos_x, y=pos_y, w=w, h=h)
        self.dejavu(size=10)

        if description:
            self.set_text_color(0, 0, 0)
            self.cell(w=width, h=height, txt=description)
            self.ln(10)

    def insert_text(self, path: str = '', w: int = 0, h: int = 7, colour: tuple = (0, 0, 0), prefix: str = None):
        text = self.load_text(path)
        r, g, b = colour
        self.set_text_color(r=r, g=g, b=b)
        for item in text:
            compiled = copy(item)
            if prefix:
                compiled = prefix + item
            self.multi_cell(w=w, h=h, txt=compiled)
            self.ln(10)

    @staticmethod
    def load_text(path: str):
        assert os.path.exists(path)

        with open(path, mode='r', encoding='utf-8') as txt:
            text = txt.readlines()

        return text


class BaseReporter(abc.ABC):
    def __init__(self, data, report_settings):
        self.data = data
        self.default_opt = SettingsReport(**report_settings)

    def generate_report(self):
        for person_data in self.data:
            filename, title = self.get_filename_and_title(person_data.get('graph'))
            self.make_pdf(data=person_data, filename=filename, title=title)

    @staticmethod
    def get_filename_and_title(path_to: str) -> tuple:
        name, _ = os.path.splitext(os.path.basename(path_to))
        folder = os.path.dirname(path_to)

        filename = os.path.join(folder, name + '.pdf')
        title = ('Результат тестирования респондента', name)

        return filename, title

    @abc.abstractmethod
    def make_pdf(self, data, filename, title):
        pass
