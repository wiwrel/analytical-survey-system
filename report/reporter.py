from report.base import PDF, BaseReporter
import os


class PQVReporter(BaseReporter):
    def __init__(self, data, report_settings):
        super().__init__(data, report_settings)

    def make_pdf(self, data: dict, filename: str = 'report.pdf', title: str = ''):
        pdf = PDF(orientation='L', unit='mm', format='A4')

        image_path: str = data['graph']
        sequence = data['data'].index.to_list()

        pdf.add_page()
        pdf.header(title=title)

        pdf.insert_image(path=image_path, w=250)

        pdf.dejavu(size=16)

        pdf.insert_text(path=os.path.join(self.default_opt.text_base, self.default_opt.text_prefix))

        pdf.dejavu(size=12)

        for num, txt_name in enumerate(sequence):
            assert self.default_opt.text_map.get(txt_name) is not None

            # colour = (0, 0, 0)
            # if num <= 2:
            #     colour = (63, 142, 210)

            prefix = f'{num + 1}. '
            path = os.path.join(self.default_opt.text_base, self.default_opt.text_map.get(txt_name))

            pdf.insert_text(path=path, prefix=prefix)

        description_text = pdf.load_text(os.path.join(self.default_opt.text_base, self.default_opt.text_pic_sign))
        pdf.insert_image(path=self.default_opt.image_tpl, w=134, h=128, description=description_text[0])

        pdf.output(filename, 'F')
        print(f'Записан отчет: {filename}')


class FFMQReporter(BaseReporter):
    def __init__(self, data, report_settings):
        super().__init__(data, report_settings)

    def make_pdf(self, data, filename, title):
        pdf = PDF(orientation='P', unit='mm', format='A4')

        image_path: str = data['graph']
        sequence = data['data'].index.to_list()

        pdf.add_page()
        pdf.header(title=title)

        pdf.insert_image(path=image_path, w=165)

        pdf.dejavu(size=8)

        for num, txt_name in enumerate(sequence):
            assert self.default_opt.text_map.get(txt_name) is not None

            # colour = (0, 0, 0)
            # if num <= 2:
            #     colour = (63, 142, 210)

            prefix = f'{num + 1}. '
            path = os.path.join(self.default_opt.text_base, self.default_opt.text_map.get(txt_name))

            pdf.insert_text(path=path, prefix=prefix)

        pdf.output(filename, 'F')
        print(f'Записан отчет: {filename}')
