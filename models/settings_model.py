from pydantic import BaseModel, DirectoryPath
import typing


class SettingsItem(BaseModel):
    survey: str
    offset: int
    last_column: typing.Optional[int]
    handler: typing.Any
    graph: typing.Any = None
    report: tuple = None


class Settings(BaseModel):
    whole: typing.List[SettingsItem]


class Options(BaseModel):
    path: DirectoryPath
    output: DirectoryPath
    settings: Settings
