from dataclasses import dataclass
from typing import List, Dict
from pandas import DataFrame
from pydantic import BaseModel


class Formula(BaseModel):
    operand: str = ''
    factor: float = None


class Column(BaseModel):
    column: str
    values: List[int]
    formula: Formula = None


class Model(BaseModel):
    model: List[Column]


class PlotDf(BaseModel):
    issue: str
    df: DataFrame

    class Config:
        arbitrary_types_allowed = True


class PlotData(BaseModel):
    plots: List[PlotDf]
