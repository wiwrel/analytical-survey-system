from pydantic import BaseModel, FilePath, DirectoryPath
import typing


class Font(BaseModel):
    path: FilePath
    family: str


class SettingsReport(BaseModel):
    width: int
    height: int
    text_base: DirectoryPath
    text_map: typing.Dict[str, str]
    image_tpl: str = None
    text_pic_sign: str = None
    text_prefix: str = None
