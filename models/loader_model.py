from pydantic import BaseModel
from models.custom_types import PandasDataFrame
import typing


class LoaderModel(BaseModel):
    from models.settings_model import SettingsItem

    filename: str
    path: str
    ext: str
    survey: SettingsItem = None
    data: PandasDataFrame = None

    class Config:
        arbitrary_types_allowed = True


class LMWrapper(BaseModel):
    wrapper: typing.List[LoaderModel] = []
