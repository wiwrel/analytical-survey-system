from plotnine import *
import pandas as pd
import os
from typing import List
import datetime
import abc


class BasePlot(abc.ABC):
    def __init__(self, dataframe, path):
        self.data = dataframe
        self.f2w = self._prep(path=path)

    @abc.abstractmethod
    def draw(self):
        pass

    @staticmethod
    def _prep(path):
        folder_to_write = os.path.join(path, f"graph_result_{datetime.datetime.now().strftime('%d-%m-%Y_(%H%M%S)')}")
        if not os.path.exists(folder_to_write):
            os.makedirs(folder_to_write)

        return folder_to_write


class PqvPlot(BasePlot):
    text = {
        'title': 'Рейтинг важности ценностных ориентаций для респондент{}',
        'x': 'Место в рейтинге',
        'y': 'Важность (%)',
        'colour_short': 'Нормативные идеалы (%)',
        'size_short': 'Разрывы между нормативными и личными приоритетами (%)'
    }

    def __init__(self, dataframe, path):
        super().__init__(dataframe, path)

    def draw(self) -> List[dict]:
        merged = []
        to_final = []

        for person in self.data.plots:
            merged.append(person.df)
            filename = os.path.join(self.f2w, f'{person.issue}.png')

            to_final.append({'data': person.df, 'graph': filename})

            suffix = 'ов' if person.issue == 'grouping' else 'а'

            plot = ggplot(aes(y='Stp', x='Rate', color='Nip', size='RAZp'), person.df) + geom_point()
            plot = plot + labs(title=self.text.get('title').format(suffix),
                               x=self.text.get('x').format(suffix),
                               y=self.text.get('y').format(suffix),
                               color=self.text.get('colour_short'),
                               size=self.text.get('size_short'))
            plot = plot + scale_x_continuous(breaks=range(-1, 12, 1), limits=[0, 11])
            plot = plot + scale_y_continuous(breaks=range(0, 101, 10), limits=[0, 100])
            plot = plot + geom_label(aes(label='translate'), data=person.df,
                                     nudge_y=-4, alpha=0.7, size=10, color='black')

            plot.save(filename, verbose=False, width=14, height=12)

        grouping_data = pd.concat(merged, ignore_index=True)

        g_filename = os.path.join(self.f2w, 'grouping.png')
        g_plot = (ggplot(aes(y='Stp', x='Rate', color='Nip', size='RAZp'),
                         grouping_data) + geom_point()
                  + scale_x_continuous(limits=[0, 10])
                  + scale_y_continuous(limits=[0, 100]))
        g_plot.save(g_filename, verbose=False, width=14, height=12)

        return to_final


class SaqPlot(BasePlot):
    def __init__(self, dataframe, path):
        super().__init__(dataframe, path)

    def draw(self):
        for person in self.data.plots:
            filename = os.path.join(self.f2w, f'{person.issue}.png')

            plot = ggplot(person.df, aes(x='translate', y=person.issue)) + geom_col()
            plot = plot + labs(title='Персональная диаграмма SAQ (%)', x=None, y=None)
            plot = plot + scale_y_continuous(breaks=range(0, 101, 10), limits=[0, 100])
            plot = plot + theme(axis_text_x=element_text(rotation=45, hjust=1, size=16),
                                axis_text_y=element_text(size=16), strip_text_y=element_text(size=20),
                                title=element_text(size=24))

            plot.save(filename=filename, verbose=False, width=19, height=16)

        return None


class FfmqPlot(BasePlot):
    def __init__(self, dataframe, path):
        super().__init__(dataframe, path)

    def draw(self):
        to_final = []

        for person in self.data.plots:
            filename = os.path.join(self.f2w, f'{person.issue}.png')

            to_final.append({'data': person.df, 'graph': filename})

            plot = ggplot(person.df, aes(x='translate', y=person.issue)) + geom_col()
            plot = plot + labs(title='Персональная диаграмма', x=None, y=None)
            plot = plot + scale_y_continuous(breaks=range(0, 41, 5), limits=[0, 40])
            plot = plot + theme(axis_text_x=element_text(rotation=45, hjust=1, size=16),
                                axis_text_y=element_text(size=16), strip_text_y=element_text(size=20),
                                title=element_text(size=24))

            plot.save(filename=filename, verbose=False, width=19, height=16)

        return to_final


class MbiPlot(BasePlot):
    def __init__(self, dataframe, path):
        super().__init__(dataframe, path)

    def draw(self):
        pass
