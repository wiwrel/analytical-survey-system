import os
import pandas as pd
from models.loader_model import LoaderModel, LMWrapper
from models.settings_model import Options
import re


class Loader:
    _ext = {
        'csv': {'worker': pd.read_csv, 'engine': 'c'},
        'xls': {'worker': pd.read_excel, 'engine': 'xlrd'},
        'xlsx': {'worker': pd.read_excel, 'engine': 'openpyxl'}
    }

    def __init__(self, option: Options):
        self.option = option
        self.loaded: LMWrapper = LMWrapper()

    def get_files(self):
        for root, _, files in os.walk(self.option.path):
            for src in files:
                _, ext = os.path.splitext(src)
                ext = ext[1:]

                item = LoaderModel(filename=src,
                                   path=os.path.join(root, src),
                                   ext=ext)

                self.loaded.wrapper.append(item)

    def get_surveys(self):
        for _files in self.loaded.wrapper:
            for item in self.option.settings.whole:
                if re.search(item.survey, _files.filename):
                    _files.survey = item
                    break

    def __load(self, path, ext):
        try:
            assert ext in self._ext.keys()
            opener = self._ext[ext].get('worker')
            engine = self._ext[ext].get('engine')
            return opener(path, engine=engine)
        except AssertionError:
            print(f"""\nНеподдерживаемый формат файла.
                Может быть только один из {', '.join(self._ext.keys())}""")
            return None

    def fill_dataframes_data(self):
        for _files in self.loaded.wrapper:
            data = self.__load(_files.path, _files.ext)
            if not data.empty:
                _files.data = data

    def load(self):
        self.get_files()
        self.get_surveys()
        self.fill_dataframes_data()
        return self.loaded
