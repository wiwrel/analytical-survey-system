tpl = [
    {'column': 'observation',
     'values': [1, 6, 11, 15, 20, 26, 31, 36],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'description',
     'values': [2, 7, 12, 16, 22, 27, 32, 37],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'action_understanding',
     'values': [5, 8, 13, 18, 23, 34, 38],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'impartial_relation_to_exp',
     'values': [3, 10, 17, 25, 30, 35, 39],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'non_responsive',
     'values': [9, 19, 21, 24, 29, 33],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
]

converter_map = {"Никогда или очень редко": 1,
                 "Редко": 2,
                 "Иногда": 3,
                 "Часто": 4,
                 "Очень часто, почти всегда": 5
                 }

reversed_indexes = [3, 5, 8, 10, 12, 13, 14, 16, 17, 18, 23, 25, 28, 30, 34, 35, 38, 39]
