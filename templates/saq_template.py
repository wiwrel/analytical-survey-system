tpl = [{'column': 'atm_command_work',
        'values': [1, 2, 3, 4, 5, 6],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'safety_atm',
        'values': [7, 8, 9, 10, 11, 12, 13],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'satisfaction_job',
        'values': [15, 16, 17, 18, 19],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'recognizing_stress',
        'values': [20, 21, 22, 23],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'direct_leader_perception',
        'values': [24, 25, 26, 27, 28],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'clinic_leader_perception',
        'values': [29, 30, 31, 32, 33],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'job_conditions',
        'values': [34, 35, 36, 37],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'proposal',
        'values': [14],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'nurse_interaction',
        'values': [38],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'doctor_interaction',
        'values': [39],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'pharma_interaction',
        'values': [40],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }},
       {'column': 'misunderstanding',
        'values': [41],
        'formula': {
            'operand': 'mean_to_pct',
            'factor': None
        }}]

converter_map = {"Совершенно не согласен (-на)": 1,
                 "Скорее не согласен (-на)": 2,
                 "Трудно сказать, согласен (-на) или не согласен (-на)": 3,
                 "Скорее согласен (-на)": 4,
                 "Совершенно согласен (-на)": 5,
                 "Не применимо": 0}

reversed_indexes = [2, 11, 36]
