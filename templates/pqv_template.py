tpl = [
    {
        'column': 'Conf_NI',
        'values': [11, 20, 40, 47],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Tra_NI',
        'values': [18, 32, 36, 44, 51],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Dobr_NI',
        'values': [33, 45, 49, 52, 54],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Uni_NI',
        'values': [1, 17, 24, 26, 29, 30, 35, 38],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Sam_NI',
        'values': [5, 16, 31, 41, 53],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Sti_NI',
        'values': [9, 25, 37],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Ged_NI',
        'values': [4, 50, 57],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Dost_NI',
        'values': [34, 39, 43, 55],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Vlas_NI',
        'values': [3, 12, 27, 46],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Bez_NI',
        'values': [8, 13, 15, 22, 56],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Zre_NI',
        'values': [2, 10, 14, 19, 21, 28, 48],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Soc_NI',
        'values': [6, 7, 23, 42],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Conf_Nip',
        'values': [98],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Tra_Nip',
        'values': [99],
        'formula': {
            'operand': 'mul',
            'factor': 2
        }
    },
    {
        'column': 'Dobr_Nip',
        'values': [100],
        'formula': {
            'operand': 'mul',
            'factor': 2
        }
    },
    {
        'column': 'Uni_Nip',
        'values': [101],
        'formula': {
            'operand': 'mul',
            'factor': 1.25
        }
    },
    {
        'column': 'Sam_Nip',
        'values': [102],
        'formula': {
            'operand': 'mul',
            'factor': 2
        }
    },
    {
        'column': 'Sti_Nip',
        'values': [103],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 3
        }
    },
    {
        'column': 'Ged_Nip',
        'values': [104],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 3
        }
    },
    {
        'column': 'Dost_Nip',
        'values': [105],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Vlas_Nip',
        'values': [106],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Bez_Nip',
        'values': [107],
        'formula': {
            'operand': 'mul',
            'factor': 2
        }
    },
    {
        'column': 'Zre_Nip',
        'values': [108],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 7
        }
    },
    {
        'column': 'Soc_Nip',
        'values': [109],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Conf_LP',
        'values': [64, 73, 85, 93],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Tra_LP',
        'values': [66, 77, 82, 95],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Dobr_LP',
        'values': [69, 75, 84, 90],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Uni_LP',
        'values': [60, 65, 76, 80, 86, 97],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Sam_LP',
        'values': [58, 68, 79, 91],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Sti_LP',
        'values': [63, 72, 87],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Ged_LP',
        'values': [67, 83, 94],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Dost_LP',
        'values': [61, 70, 81, 89],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Vlas_LP',
        'values': [59, 74, 96],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Bez_LP',
        'values': [62, 71, 78, 88, 92],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Conf_LPp',
        'values': [122],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Tra_LPp',
        'values': [123],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Dobr_LPp',
        'values': [124],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Uni_LPp',
        'values': [125],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 6
        }
    },
    {
        'column': 'Sam_LPp',
        'values': [126],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Sti_LPp',
        'values': [127],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 3
        }
    },
    {
        'column': 'Ged_LPp',
        'values': [128],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 3
        }
    },
    {
        'column': 'Dost_LPp',
        'values': [129],
        'formula': {
            'operand': 'mul',
            'factor': 2.5
        }
    },
    {
        'column': 'Vlas_LPp',
        'values': [130],
        'formula': {
            'operand': 'mul',
            'factor': 10 / 3
        }
    },
    {
        'column': 'Bez_LPp',
        'values': [131],
        'formula': {
            'operand': 'mul',
            'factor': 2
        }
    },
    {
        'column': 'Conf_RAZp',
        'values': [110, 132],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Tra_RAZp',
        'values': [111, 133],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Dobr_RAZp',
        'values': [112, 134],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Uni_RAZp',
        'values': [113, 135],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Sam_RAZp',
        'values': [114, 136],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Sti_RAZp',
        'values': [115, 137],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Ged_RAZp',
        'values': [116, 138],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Dost_RAZp',
        'values': [117, 139],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Vlas_RAZp',
        'values': [118, 140],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Bez_RAZp',
        'values': [119, 141],
        'formula': {
            'operand': 'minus',
            'factor': None
        }
    },
    {
        'column': 'Abs_Sum_RAZ',
        'values': [142, 143, 144, 145, 146, 147, 148, 149, 150, 151],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Av_Sum_RAZ',
        'values': [152],
        'formula': {
            'operand': 'mul',
            'factor': 0.1
        }
    },
    {
        'column': 'Conf_Stp',
        'values': [110, 132],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Tra_Stp',
        'values': [111, 133],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Dobr_Stp',
        'values': [112, 134],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Uni_Stp',
        'values': [113, 135],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Sam_Stp',
        'values': [114, 136],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Sti_Stp',
        'values': [115, 137],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Ged_Stp',
        'values': [116, 138],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Dost_Stp',
        'values': [117, 139],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Vlas_Stp',
        'values': [118, 140],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Bez_Stp',
        'values': [119, 141],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Amplitude_Value',
        'values': [154, 155, 156, 157, 158, 159, 160, 161, 162, 163],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Av_Ampl_Value',
        'values': [164],
        'formula': {
            'operand': 'mul',
            'factor': 0.1
        }
    },
    {
        'column': 'Av_Str_Opred',
        'values': [156, 157],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Av_Str_Utv',
        'values': [160, 161, 162],
        'formula': {
            'operand': 'sum_div',
            'factor': 3
        }
    },
    {
        'column': 'Av_Str_Sochr',
        'values': [154, 155, 163],
        'formula': {
            'operand': 'sum_div',
            'factor': 3
        }
    },
    {
        'column': 'Av_Str_Otkr',
        'values': [158, 159],
        'formula': {
            'operand': 'sum_div',
            'factor': 2
        }
    },
    {
        'column': 'Aml_Rost',
        'values': [166, 169],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Aml_Zasch',
        'values': [167, 168],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Aml_Ego',
        'values': [167, 169],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    },
    {
        'column': 'Aml_Socio',
        'values': [166, 168],
        'formula': {
            'operand': 'sum',
            'factor': None
        }
    }
]
