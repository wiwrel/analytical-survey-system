from operator import gt, lt

tpl = [
    {'column': 'emotional_reduction',
     'values': [1, 2, 3, 8, 13, 14, 16, 20],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'depersonalization',
     'values': [5, 10, 11, 15, 22],
     'formula': {
         'operand': 'sum',
         'factor': None
     }},
    {'column': 'professional_reduction',
     'values': [4, 7, 9, 12, 17, 18, 19, 21],
     'formula': {
         'operand': 'sum',
         'factor': None
     }}
]

resume_map = {
    'emotional_reduction': {"min": (lt, 15),
                            "max": (gt, 25)},
    'depersonalization': {"min": (lt, 5),
                          "max": (gt, 11)},
    'professional_reduction': {"min": (gt, 37),
                               "max": (lt, 30)}
}

converter_map = {"Никогда": 0,
                 "Очень редко": 1,
                 "Редко": 2,
                 "Иногда": 3,
                 "Часто": 4,
                 'Очень часто': 5,
                 'Каждый день': 6
                 }

reversed_indexes = []
