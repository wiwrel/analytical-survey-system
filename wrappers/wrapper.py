from abc import abstractmethod, ABC
import pandas as pd
import os
import datetime


class AbstractSurvey(ABC):
    from models.loader_model import LoaderModel
    from models.settings_model import Options, SettingsItem

    def __init__(self, survey_object: LoaderModel, option: Options, survey_settings: SettingsItem):
        self.survey_object = survey_object
        self.option = option
        self.survey_settings = survey_settings

    @abstractmethod
    def get(self):
        pass

    def write(self, df: pd.DataFrame):
        path_to_write = os.path.join(self.option.output, self.survey_settings.survey)
        try:
            assert os.path.exists(path_to_write)
        except AssertionError:
            os.makedirs(path_to_write)

        filename = f"{self.survey_settings.survey.upper()} result {datetime.datetime.now().strftime('%d-%m-%Y (%H%M%S)')}.xls"

        output = os.path.join(path_to_write, filename)

        df.to_excel(output, index=False, engine='openpyxl')

        print(f'\nРезультаты записаны в файл {output}')

        return path_to_write


class SurveyMain(object):
    from models.loader_model import LoaderModel
    from models.settings_model import Options

    def __init__(self, survey_object: LoaderModel, option: Options):
        self.survey_object = survey_object
        self.option = option
        self.survey_settings = [x for x in option.settings.whole if
                                x.survey == survey_object.survey.survey][0]

    def solve(self):
        entity = SurveySolver(survey_object=self.survey_object,
                              option=self.option,
                              survey_settings=self.survey_settings)
        entity.get()


class SurveySolver(AbstractSurvey):
    """docstring for Survey"""

    def __init__(self, survey_object, option, survey_settings):
        super().__init__(survey_object, option, survey_settings)
        self.survey_model = self.survey_settings.handler(self.survey_object, self.survey_settings)

    def get(self):
        """
            Use PQV by default
        """
        print("""Параметры загружены. Используемые параметры:\n\n"""
              f"    Путь до исходника: '{self.option.path}'\n"
              f"    Путь до файла с результатами: '{self.option.output}'\n")

        self.survey_model.model()
        print('\nРасчет модели завершен.')

        path = self.write(self.survey_model.df)
        print(f'\nИтоговые результаты записаны в {path}')

        if self.survey_settings.graph:
            print('\nРисуем графики по респондентам')
            if not self.survey_model.is_calculated:
                print("""Не был совершен расчет модели.
                    Выполните метод класса `model`. Прерываю...""")
                return
            plot_handler = self.survey_settings.graph

            raw_plot = plot_handler(dataframe=self.survey_model.for_plot(), path=path)
            middle_data = raw_plot.draw()

            if self.survey_settings.report and middle_data:
                print('\nФормируем текстовые отчеты')
                report_handler, report_setup = self.survey_settings.report
                report = report_handler(data=middle_data, report_settings=report_setup)
                report.generate_report()
