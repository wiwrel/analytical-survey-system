import argparse
import os
from wrappers.wrapper import SurveyMain
from loader import Loader
from models.loader_model import LMWrapper
from settings import settings
from models.settings_model import Settings, Options


def parse_arguments():
    p = argparse.ArgumentParser(
        description="""Система для автоматизированного анализа
        результатов опросов врачей и иных респондентов по
        специализированным опросникам""")
    p.add_argument("-p", "--path", type=str, default='data/source/',
                   help="""Путь к папке с исходными файлами.
                        По умолчанию 'data/source/'""")
    p.add_argument("-o", "--output", type=str, default='data/result/',
                   help="""Путь к папке с результатами.
                        По умолчанию 'data/result/'""")

    return p.parse_args()


def make_options(args):
    return Options(path=args.path,
                   output=args.output,
                   settings=Settings(whole=settings))


def main(args):
    option = make_options(args)

    try:
        assert os.path.exists(option.path) and os.path.isdir(option.path)
    except AssertionError:
        print('\nПапка с исходными файлами не существует или указан не верный путь')

    _surveys: Loader = Loader(option)
    surveys: LMWrapper = _surveys.load()

    for survey in surveys.wrapper:
        entity = SurveyMain(survey_object=survey, option=option)
        entity.solve()


if __name__ == '__main__':
    main(parse_arguments())
