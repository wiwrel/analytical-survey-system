from abc import ABC, abstractmethod
import typing
import copy
import pandas as pd
from models.calc_model import Model


class AbstractAnalytic(ABC):
    from models.loader_model import LoaderModel
    from models.settings_model import SettingsItem

    _template = None

    def __init__(self, survey_object: LoaderModel, survey_settings: SettingsItem):
        self.survey_object = survey_object
        self.survey_settings = survey_settings
        self.df = survey_object.data
        self.is_calculated = False
        self.data_model: typing.Optional[Model] = None

    @abstractmethod
    def model(self):
        pass

    @abstractmethod
    def for_plot(self):
        pass

    @property
    def get_template(self):
        if self._template is None:
            raise ValueError('"Template" object is must be defined!')
        return self._template

    def clean(self):
        _range = self.df.columns[range(self.survey_settings.offset - 1,
                                       self.survey_settings.last_column - 1)]
        self.df = self.df.dropna(subset=_range)

    def init_model(self):
        self.data_model = Model(model=self.get_template)

    def shift_comprehensions(self, source: typing.List[int]) -> typing.List[int]:
        return [x + self.survey_settings.offset - 2 for x in source]

    def prep(self, converter_map=None, reverse_map=None, need_convert=False):
        self.init_model()
        self.clean()

        if need_convert:
            if converter_map and reverse_map is not None:
                self.rosetta(converter_map, reverse_map)
            else:
                raise EnvironmentError('Not enough arguments. "converter_map" and "reverse_map" is not must be None!')

    def calc_indicators(self):
        for col in self.data_model.model:
            shifted = self.shift_comprehensions(col.values)
            self.df[col.column] = self._map[col.formula.operand](self, col.formula.factor, shifted)

    @staticmethod
    def convert_to_int(source: typing.List[str], converter_map: typing.Dict[str, int], reverse: bool = False):
        my_map = copy.deepcopy(converter_map)

        if reverse:
            zero_value = {k: v for k, v in my_map.items() if v == 0}
            if zero_value:
                for key in set(my_map).intersection(set(zero_value)):
                    del my_map[key]

            rev_keys = list(my_map.values())[::-1]
            _new_map = dict(zip(my_map.keys(), rev_keys))

            if zero_value:
                _new_map.update(zero_value)

            my_map = copy.deepcopy(_new_map)

        return [my_map.get(x) for x in source]

    def rosetta(self, converter_map, reverse_map):
        my_df = copy.deepcopy(self.df)
        columns = my_df.columns.tolist()[self.survey_settings.offset - 1:]

        for idx, column in enumerate(columns):
            column_data = my_df.loc[:, column].tolist()

            is_reverse = True if idx + 1 in reverse_map else False

            my_df.loc[:, column] = pd.Series(data=self.convert_to_int(source=column_data,
                                                                      converter_map=converter_map,
                                                                      reverse=is_reverse))

        self.df = copy.deepcopy(my_df)

    def _sum(self, factor, shifted):
        assert shifted
        return self.df.iloc[:, shifted].sum(axis=1)

    def _mul(self, factor, shifted):
        assert factor and shifted
        return self.df.iloc[:, shifted].mul(factor)

    def _minus(self, factor, shifted):
        assert len(shifted) == 2
        first, second = shifted
        return self.df.iloc[:, first] - self.df.iloc[:, second]

    def _minus_div(self, factor, shifted):
        assert len(shifted) == 2
        assert factor
        return self._minus(factor, shifted).div(factor)

    def _sum_div(self, factor, shifted):
        assert factor and shifted
        return self._sum(factor, shifted).div(factor)

    def _mean_to_pct(self, factor, shifted):
        assert shifted
        return (self.df.iloc[:, shifted].mean(axis=1) - 1) * 25

    _map = {
        'sum': _sum,
        'mul': _mul,
        'minus': _minus,
        'minus_div': _minus_div,
        'sum_div': _sum_div,
        'mean_to_pct': _mean_to_pct
    }
