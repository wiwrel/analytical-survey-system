import pandas as pd
from templates.ffmq_template import tpl, converter_map, reversed_indexes
from models.loader_model import LoaderModel
from models.settings_model import SettingsItem
from models.calc_model import PlotData, PlotDf
from surveys.base import AbstractAnalytic


class FFMQ(AbstractAnalytic):
    _template = tpl

    def __init__(self, survey_object: LoaderModel, survey_settings: SettingsItem):
        super().__init__(survey_object, survey_settings)

    def model(self):
        self.prep(converter_map=converter_map, reverse_map=reversed_indexes, need_convert=True)
        print('\n    1. Данные успешно очищены')

        print('\n    2. Расчет показателей по формулам...')
        self.calc_indicators()
        self.is_calculated = True

    def for_plot(self):
        users = self.df.loc[:, 'Адрес электронной почты'].str.replace('[-@\\.]', '_', regex=True).to_list()
        data = self.df.loc[:, 'observation':'non_responsive'].T
        data.columns = users
        translate = pd.DataFrame(list(self._translate.values()), columns=['translate'],
                                 index=list(self._translate.keys()))

        plot_data = PlotData(plots=[])

        for key, name in enumerate(users):
            data_per_user = pd.concat([data.iloc[:, key].round(2), translate], axis=1)
            plot_data.plots.append(PlotDf(issue=name, df=data_per_user))

        return plot_data

    _translate = {'observation': 'Наблюдение',
                  'description': 'Описание',
                  'action_understanding': 'Осознанность действий',
                  'impartial_relation_to_exp': 'Безоценочное отношение к опыту',
                  'non_responsive': 'Нереагирование'}
