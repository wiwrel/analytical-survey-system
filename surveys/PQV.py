import pandas as pd
from collections import namedtuple
from templates.pqv_template import tpl
from models.calc_model import PlotData, PlotDf
from copy import deepcopy
from surveys.base import AbstractAnalytic


class PQV(AbstractAnalytic):
    """docstring for PQV"""

    row_item = namedtuple('row_item', ['idx', 'rate'])
    _template = tpl

    def __init__(self, survey_object, survey_settings):
        super().__init__(survey_object, survey_settings)

    def model(self):
        print('\n    1. Очистка данных')
        self.prep()

        print('\n    2. Расчет показателей по формулам...')
        self.calc_indicators()

        print('\n    3. Расчет рейтинга...')
        self.calc_rate()
        self.is_calculated = True

    def for_plot(self):
        data = {
            'Stp': self.df.loc[:, 'Conf_Stp':'Bez_Stp'],
            'Rate': self.df.filter(regex='Rate'),
            'users': self.df.loc[:, 'Адрес электронной почты'].str.replace(
                '[-@\\.]', '_', regex=True).to_list(),
            'Nip': self.df.loc[:, 'Conf_Nip':'Bez_Nip'],
            'RAZp': self.df.loc[:, 'Conf_RAZp':'Bez_RAZp'].abs()
        }

        selected = ['Stp', 'Rate', 'Nip', 'RAZp']
        data = self.calc_mean(data, selected)
        translate = pd.DataFrame(list(self._translate.values()), columns=['translate'],
                                 index=list(self._translate.keys()))

        plot_data = PlotData(plots=[])

        for key, name in enumerate(data['users']):
            tmp_series = [translate]
            for sets in selected:
                ts = data[sets].iloc[key, :]
                ts = self.clean_index(ts)

                tmp_series.append(ts)

            tdf = pd.concat(tmp_series, axis=1)
            to_rename = selected[:]
            to_rename.insert(0, tdf.columns[0])
            tdf.columns = to_rename

            tdf = tdf.sort_values(by='Rate', ascending=False)

            item = PlotDf(issue=name, df=tdf)

            item.df['Rate'] = item.df['Rate'].astype('int64')

            plot_data.plots.append(item)

        return plot_data

    def calc_rate(self):
        i_range = range(self.df.columns.get_loc('Conf_Stp'),
                        self.df.columns.get_loc('Bez_Stp') + 1)
        tmp = self.df.iloc[:, i_range]
        rows = []
        for idx in tmp.index.to_list():
            _dict = {'stp': tmp.loc[idx, :].sort_values(ascending=False),
                     'rate': range(1, 11)}
            row = self.row_item(idx=idx,
                                rate=pd.DataFrame(_dict).T.loc['rate', :])
            rows.append(row)

        rates, indexes = [i.rate for i in rows], [i.idx for i in rows]

        result = pd.concat(rates, axis=1).T

        names = [x.split('_')[0] + '_Rate' for x in result.columns.to_list()]

        result.columns, result.index = names, indexes

        self.df = self.df.join(result)

    @staticmethod
    def calc_mean(data: dict, selected: list):
        temp = deepcopy(data)
        temp['users'].append('group_mean')

        for var in selected:
            df = pd.concat([temp[var].T, temp[var].mean()], axis=1).T
            df.index = [x for x in range(len(df.index))]
            temp[var] = df

        return temp

    @staticmethod
    def clean_index(series: pd.Series) -> pd.Series:
        temp = deepcopy(series)
        temp.index = [x.split('_')[0] for x in series.index]
        return temp

    _translate = {
        'Conf': 'Конформность',
        'Tra': 'Традиция',
        'Dobr': 'Доброта',
        'Uni': 'Универсализм',
        'Sam': 'Самостоятельность',
        'Sti': 'Стимуляция',
        'Ged': 'Гедонизм',
        'Dost': 'Достижение',
        'Vlas': 'Власть',
        'Bez': 'Безопасность'
    }
