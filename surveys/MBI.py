import pandas as pd
from templates.mbi_template import tpl, converter_map, reversed_indexes, resume_map
from models.loader_model import LoaderModel
from models.settings_model import SettingsItem
from surveys.base import AbstractAnalytic


class MBI(AbstractAnalytic):
    _template = tpl

    def __init__(self, survey_object: LoaderModel, survey_settings: SettingsItem):
        super().__init__(survey_object, survey_settings)

    def model(self):
        pass
        self.prep(converter_map=converter_map, reverse_map=reversed_indexes, need_convert=True)
        print('\n    1. Данные успешно очищены')

        print('\n    2. Расчет показателей по формулам...')
        self.calc_indicators()
        self.calc_resume()
        self.calc_integral_index()

        self.is_calculated = True

    def calc_resume(self):
        for column_name, logic in resume_map.items():
            resume_result = []
            exported = self.df.loc[:, column_name].tolist()

            max_func, max_val = logic['max']
            min_func, min_value = logic['min']

            for row in exported:
                if min_func(row, min_value):
                    resume_result.append('низкий')
                elif max_func(row, max_val):
                    resume_result.append('высокий')
                else:
                    resume_result.append('средний')

            new_name = f"Resume_{column_name}"
            self.df[new_name] = pd.Series(resume_result, index=self.df.index[: len(resume_result)])

    def calc_integral_index(self):
        self.df['integral burnout index'] = pow(((self.df['emotional_reduction'] / 54) ** 2
                                                 + (self.df['depersonalization'] / 30) ** 2
                                                 + (1 - self.df['professional_reduction'] / 48) ** 2) / 3, 0.5)

    def for_plot(self):
        pass
